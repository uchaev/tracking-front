import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import './scss/style.scss';
import PrivateRoute from './views/profile/lk'
import {useSelector} from 'react-redux'
import Cookies from "js-cookie";
import {AppState} from "./store/types/store-types";

declare module 'react-redux' {
  interface DefaultRootState extends AppState {
  }
}

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const TheLayout = React.lazy(() => import('./containers/TheLayout'));

// Pages
const Login = React.lazy(() => import('./views/login/Login'));
const Register = React.lazy(() => import('./views/register/Register'));


const App = () => {
  const isAuth = useSelector((state) => {
    return !!state.user.token || !!Cookies.get('token')
  });

  return (
    <BrowserRouter>
      <React.Suspense fallback={loading}>
        <Switch>
          <Route exact path="/login" render={props => <Login {...props}/>}/>
          <Route exact path="/register" render={props => <Register {...props}/>}/>
          <PrivateRoute authed={isAuth} path='/' component={TheLayout}/>
        </Switch>
      </React.Suspense>
    </BrowserRouter>
  );
}

export default App;
