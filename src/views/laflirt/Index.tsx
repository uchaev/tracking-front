import React, {useCallback, useEffect, useState} from 'react'
import {
  CBadge, CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable, CNavItem, CNavLink,
  CRow
} from '@coreui/react'
import {getLaflirtOrders, delLaflirtById} from "../../api/services/services";
import {LaflirtOrders} from "../../api/services/types/services-types";
import {Link} from "react-router-dom";

const fields = ['id', 'phone', 'cart', 'createdAt', 'view', 'delete']


const Tables = () => {
  const [boards, setBoards] = useState<[] | LaflirtOrders>([])

  const fetchLaflirt = useCallback(() => {
    getLaflirtOrders().then((data) => {
      setBoards(data)
    })
  }, []);


  const onRemove = (id: any) => {
    delLaflirtById(id).then(() => {
      setBoards(boards.filter(item => item.id !== id))
    })
  }

  useEffect(() => {
    fetchLaflirt()
  }, [fetchLaflirt])

  return (
    <>
      <CRow>
        <CCol xs="12" lg="12">
          <CCard>
            <CCardHeader>
              Orders
            </CCardHeader>
            <CCardBody>
              <CDataTable
                items={boards}
                fields={fields}
                itemsPerPage={5}
                pagination
                scopedSlots={{
                  'view':
                    (item: any) => (
                      <td>
                        <Link to={`laflirt/${item.id}`}>
                          <CButton block color="info">view</CButton>
                        </Link>
                      </td>
                    ),
                  'delete':
                    (item: any) => (
                      <td>
                        <CButton onClick={() => {
                          onRemove(item.id)
                        }} block color="danger">delete</CButton>
                      </td>
                    )
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default Tables
