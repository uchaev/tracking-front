import React, {useCallback, useEffect, useState} from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol, CDataTable,
  CForm,
  CFormGroup,
  CInput, CInvalidFeedback, CLabel,
  CRow, CToast, CToastBody, CToaster, CToastHeader, CValidFeedback
} from '@coreui/react'


import {
  CChartLine,
} from '@coreui/react-chartjs'

import CIcon from '@coreui/icons-react'
import * as yup from "yup";
import {Formik} from "formik";
import {
  getLaflirtOrderById,
  sendToIntimShop,
  getIntimShopOrdersByLaflirtId,
  updateLaflirtOrder
} from "../../../api/services/services";
import {
  Boards,
  Rates,
  Board,
  LaflirtOrder,
  IntinShopOrders,
  IntinShopOrder
} from "../../../api/services/types/services-types";
import {Link} from "react-router-dom";

const fields = ['id', 'response', 'createdAt']

const schema = yup.object({
  phone: yup.string().required('phone is required').min(3),
  cart: yup.string().required('cart is required').min(3),
  comment: yup.string(),
  note: yup.string()
});

const BasicForms = ({match}: any) => {
  const [lafliert, setLaflirt] = useState<LaflirtOrder | null>(null)
  const [intimShops, setIntimShops] = useState<IntinShopOrders | []>([])


  const onSendToIntimShop = () => {

    let items: any = []
    const cart = lafliert!.cart.split('@')

    cart.forEach((item) => {
      let id = null
      let count = null
      item.split(',').forEach((item) => {
        if (item.indexOf('id=') > -1) {
          id = item.replace('id=', '')
        }
        if (item.indexOf('count=') > -1) {
          count = item.replace('count=', '')
        }
      })

      items.push(`${id}:${count}`)
    })

    sendToIntimShop({id: lafliert!.id, phone: lafliert!.id, items: items.join(','), comment: lafliert!.comment})
      .then((data: IntinShopOrder) => {
        alert('Успех')
        getIntimShopOrdersByLaflirtId(match.params.id).then((data) => {
          setIntimShops(data)
        })
      }).catch((e) => {
      alert(e.message)
    })
  }

  const fetchIntimShops = useCallback(() => {
    getIntimShopOrdersByLaflirtId(match.params.id).then((data) => {
      setIntimShops(data)
    })
  }, []);


  const fetchLaflirtOrder = useCallback(() => {
    getLaflirtOrderById(match.params.id).then((data) => {
      setLaflirt(data)
    })
  }, []);


  useEffect(() => {
    fetchLaflirtOrder()
    fetchIntimShops()
  }, [fetchLaflirtOrder, fetchIntimShops])

  return (
    <>
      <CRow>
        {lafliert ? (<CCol xs="6" sm="6">
          <Formik initialValues={{
            phone: lafliert.phone,
            cart: lafliert.cart,
            id: lafliert.id,
            comment: lafliert.comment,
            note: lafliert.note
          }}
                  validationSchema={schema}
                  onSubmit={(values) => {
                    updateLaflirtOrder({...values, id: lafliert!.id}).then(() => {
                      alert('Обновлен')
                      getLaflirtOrderById(match.params.id).then((data) => {
                        setLaflirt(data)
                      })
                    })
                  }}>
            {({isSubmitting, values, handleSubmit, touched, handleChange, errors, handleBlur}) => (
              <CCard>
                <CCardHeader>
                  Detail order
                </CCardHeader>
                <CCardBody>
                  <CFormGroup>
                    <CLabel htmlFor="inputIsValid">Phone*</CLabel>
                    <CInput onBlur={handleBlur('phone')} onInput={handleChange('phone')} defaultValue={values.phone}
                            id="inputIsValid"/>
                    {errors.phone && touched.phone ? (
                      <div>{errors.phone}</div>
                    ) : null}
                  </CFormGroup>
                  <CFormGroup>
                    <CLabel htmlFor="inputIsInvalid">Cart*</CLabel>
                    <CInput onBlur={handleBlur('cart')} onInput={handleChange('cart')} defaultValue={values.cart}
                            id="inputIsInvalid"/>
                    {errors.cart && touched.cart ? (
                      <div>{errors.cart}</div>
                    ) : null}
                  </CFormGroup>

                  <CFormGroup>
                    <CLabel htmlFor="inputIsInvalid">Comment</CLabel>
                    <CInput onBlur={handleBlur('comment')} onInput={handleChange('comment')}
                            defaultValue={values.comment}
                            id="inputIsInvalid"/>
                    {errors.comment && touched.comment ? (
                      <div>{errors.cart}</div>
                    ) : null}
                  </CFormGroup>


                  <CFormGroup>
                    <CLabel htmlFor="inputIsInvalid">Note</CLabel>
                    <CInput onBlur={handleBlur('note')} onInput={handleChange('note')} defaultValue={values.note}
                            id="inputIsInvalid"/>
                    {errors.note && touched.note ? (
                      <div>{errors.note}</div>
                    ) : null}
                  </CFormGroup>

                </CCardBody>
                <CCardFooter>
                  <CButton onClick={handleSubmit} type="submit" color="success"><CIcon
                    name="cil-scrubber"/> Update</CButton>
                </CCardFooter>
              </CCard>
            )}
          </Formik>
        </CCol>) : ''}
        <CCol xs="6" sm="6">
          {lafliert && (<CCard>
            <CCardHeader>Create drafts in intimshop</CCardHeader>
            <CCardBody>
              <CDataTable
                items={intimShops}
                fields={fields}
                itemsPerPage={5}
                pagination
              />
              <br/>
              <CButton onClick={onSendToIntimShop} type="submit" color="success"><CIcon
                name="cil-scrubber"/> Send to</CButton>
            </CCardBody>
          </CCard>)}
        </CCol>
      </CRow>
    </>
  )
}

export default BasicForms
