import React, {useState} from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CInput, CInvalidFeedback, CLabel,
  CRow, CValidFeedback
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import * as yup from "yup";
import {Formik} from "formik";
import {Redirect} from 'react-router-dom'
import {createBoard} from "../../../api/services/services";

const schema = yup.object({
  title: yup.string().required('title is required').min(3),
  desc: yup.string().required('desc is required').min(3),
});

interface MyFormValues {
  title: string;
  desc: string;
}

const initialValues: MyFormValues = {title: '', desc: ''};


const BasicForms = () => {

  const [redirect, setRedirect] = useState(false)
  if (redirect) {
    return <Redirect to={'/boards'}/>
  }
  return (
    <>
      <CRow>
        <CCol xs="12" sm="6">
          <Formik initialValues={initialValues} validationSchema={schema} onSubmit={(values) => {
            createBoard(values).then((data) => {
              setRedirect(true)
            }).catch((e) => {
              // do nothing
            })
          }}>
            {({isSubmitting, values, handleSubmit, touched, handleChange, errors, handleBlur}) => (
              <CCard>
                <CCardHeader>
                  Create bord
                </CCardHeader>
                <CCardBody>
                  <CFormGroup>
                    <CLabel htmlFor="inputIsValid">Title</CLabel>
                    <CInput onBlur={handleBlur('title')} onInput={handleChange('title')} defaultValue={values.title}
                            id="inputIsValid"/>
                    {errors.title && touched.title ? (
                      <div>{errors.title}</div>
                    ) : null}
                  </CFormGroup>
                  <CFormGroup>
                    <CLabel htmlFor="inputIsInvalid">Desc</CLabel>
                    <CInput onBlur={handleBlur('desc')} onInput={handleChange('desc')} defaultValue={values.desc}
                            id="inputIsInvalid"/>
                    {errors.desc && touched.desc ? (
                      <div>{errors.desc}</div>
                    ) : null}
                  </CFormGroup>
                </CCardBody>
                <CCardFooter>
                  <CButton onClick={handleSubmit} type="submit" color="success"><CIcon
                    name="cil-scrubber"/> Submit</CButton>
                </CCardFooter>
              </CCard>
            )}
          </Formik>
        </CCol>
      </CRow>
    </>
  )
}

export default BasicForms
