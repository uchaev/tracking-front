import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText, CInvalidFeedback,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import React, {FunctionComponent, useState} from 'react'
import {authRegister} from "../../api/services/services";
import {addToken} from "../../store/actions/user";
import {useDispatch} from 'react-redux'
import {Redirect} from 'react-router-dom'
import Cookies from 'js-cookie'
import {RouteComponentProps} from "react-router";

const Register: FunctionComponent<RouteComponentProps> = () => {

  const dispatch = useDispatch();

  const [redirect, setRedirect] = useState(false)
  const [email, setEmail] = useState('')
  const [name, setName] = useState('')
  const [password, setPassword] = useState('')
  const [passwordConfirm, setPasswordConform] = useState('')


  const usernameHandler = (event: any) => {
    setName(event.target.value)
  }

  const emailHandler = (event: any) => {
    setEmail(event.target.value)
  }

  const passwordHandler = (event: any) => {
    setPassword(event.target.value)
  }

  const passwordConformHandler = (event: any) => {
    setPasswordConform(event.target.value)
  }

  const reg = () => {
    const data = {
      email,
      name,
      password
    }
    authRegister(data)
      .then(({token}) => {
        dispatch(addToken(token))
        Cookies.set('token', token)
        setRedirect(true)
      })
      .catch((e) => {
        // do noting
      })
  }

  if (redirect) {
    return <Redirect to='/'/>;
  } else {
    return (
      <div className="c-app c-default-layout flex-row align-items-center">
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="9" lg="7" xl="6">
              <CCard className="mx-4">
                <CCardBody className="p-4">
                  <CForm>
                    <h1>Register</h1>
                    <p className="text-muted">Create your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user"/>
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput onInput={usernameHandler} type="text" placeholder="Username" autoComplete="username"/>
                    </CInputGroup>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>@</CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput onInput={emailHandler} type="text" placeholder="Email" autoComplete="email"/>
                      <CInvalidFeedback className="help-block">
                        Please provide a valid information
                      </CInvalidFeedback>
                    </CInputGroup>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked"/>
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput onInput={passwordHandler} type="password" placeholder="Password"
                              autoComplete="new-password"/>
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked"/>
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput onInput={passwordConformHandler} type="password" placeholder="Repeat password"
                              autoComplete="new-password"/>
                    </CInputGroup>
                    <CButton onClick={reg} color="success" block>Create Account</CButton>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
        </CContainer>
      </div>
    )
  }


}

export default Register
