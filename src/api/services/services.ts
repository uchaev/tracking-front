import client from "../client/client";
import {
  authReg,
  authLog,
  authToken,
  addBoard,
  successAddBoard,
  Boards,
  Rates,
  Board,
  LaflirtOrders,
  LaflirtOrder,
  IntinShopOrders,
  IntinShopOrder,
} from "./types/services-types";
import {AxiosResponse} from "axios";
import Cookies from "js-cookie";
// Регистрация
export const authRegister = (data: authReg) => {
  return new Promise<authToken>((resolve, reject) => {
    client({url: 'auth/register', method: 'post', data})
      .then((response: AxiosResponse<authToken>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}

// Авторизация
export const authLogin = (data: authLog) => {
  return new Promise<authToken>((resolve, reject) => {
    client({url: 'auth/login', method: 'post', data})
      .then((response: AxiosResponse<authToken>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}
// Создание доски
export const createBoard = (data: addBoard) => {
  return new Promise<successAddBoard>((resolve, reject) => {
    client({
      url: 'profile/boards', method: 'post', data, headers: {
        'Authorization': `Bearer ${_getToken()}`
      }
    })
      .then((response: AxiosResponse<successAddBoard>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}

export const updateBoard = (data: addBoard) => {
  return new Promise<successAddBoard>((resolve, reject) => {
    client({
      url: 'profile/boards', method: 'put', data, headers: {
        'Authorization': `Bearer ${_getToken()}`
      }
    })
      .then((response: AxiosResponse<successAddBoard>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}

// Получение досок
export const getBoards = () => {
  return new Promise<Boards>((resolve, reject) => {
    client({
      url: 'profile/boards', method: 'get', headers: {
        'Authorization': `Bearer ${_getToken()}`
      }
    })
      .then((response: AxiosResponse<Boards>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}

// Получение доски по id
export const getBoardById = (id: number) => {
  return new Promise<Board>((resolve, reject) => {
    client({
      url: 'profile/boards', method: 'get', params: {
        id
      }, headers: {
        'Authorization': `Bearer ${_getToken()}`
      }
    })
      .then((response: AxiosResponse<Board>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}

// Удаление доски
export const deleteBoards = (id: number) => {
  return new Promise<any>((resolve, reject) => {
    client({
      url: `profile/boards/${id}`, method: 'delete', headers: {
        'Authorization': `Bearer ${_getToken()}`
      }
    })
      .then((response: AxiosResponse<any>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}

// Получение всех оценок у доски
export const getRatesByIdBoard = (id: number) => {
  return new Promise<Rates>((resolve, reject) => {
    client({
      url: `rate`, method: 'get', params: {
        boardId: id
      }, headers: {
        'Authorization': `Bearer ${_getToken()}`
      }
    })
      .then((response: AxiosResponse<Rates>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}


// Получение заказов с laflirt
export const getLaflirtOrders = () => {
  return new Promise<LaflirtOrders>((resolve, reject) => {
    client({
      url: 'laflirt/orders', method: 'get', headers: {
        'Authorization': `Bearer ${_getToken()}`
      }
    })
      .then((response: AxiosResponse<LaflirtOrders>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}


// Получение заказов с laflirt
export const updateLaflirtOrder = (data: any) => {
  return new Promise<any>((resolve, reject) => {
    client({
      url: 'laflirt/add', method: 'put', data, headers: {
        'Authorization': `Bearer ${_getToken()}`
      }
    })
      .then((response: AxiosResponse<any>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}


// Получение заказов с laflirt
export const getLaflirtOrderById = (id: any) => {
  return new Promise<LaflirtOrder>((resolve, reject) => {
    client({
      url: 'laflirt/orders',
      method: 'get',
      params: {
        id
      },
      headers: {
        'Authorization': `Bearer ${_getToken()}`
      }
    })
      .then((response: AxiosResponse<LaflirtOrder>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}

// Отправить заказ в интимшоп
export const sendToIntimShop = (data: any) => {
  return new Promise<IntinShopOrder>((resolve, reject) => {
    client({
      url: 'intimshop/add',
      method: 'post',
      data,
      headers: {
        'Authorization': `Bearer ${_getToken()}`
      }
    })
      .then((response: AxiosResponse<IntinShopOrder>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}


// Удалить заказ
export const delLaflirtById = (id: any) => {
  return new Promise<IntinShopOrder>((resolve, reject) => {
    client({
      url: `laflirt/orders/${id}`,
      method: 'delete',
      headers: {
        'Authorization': `Bearer ${_getToken()}`
      }
    })
      .then((response: AxiosResponse<IntinShopOrder>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}


// Получить заказы из интимшопа для laflirtID

export const getIntimShopOrdersByLaflirtId = (id: any) => {
  return new Promise<IntinShopOrders>((resolve, reject) => {
    client({
      url: 'intimshop/orders',
      method: 'get',
      params: {
        laflirtId: id
      },
      headers: {
        'Authorization': `Bearer ${_getToken()}`
      }
    })
      .then((response: AxiosResponse<IntinShopOrders>) => {
        resolve(response.data)
      }).catch((e) => {
      reject(e)
      _catchError(e)
    })
  })
}


const _getToken = (): string | undefined => {
  return Cookies.get('token')
}

const _catchError = (e: any) => {
  console.log(e.message)
  // to noting
}
