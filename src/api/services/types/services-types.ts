export type authReg = {
  email: string
  password: string
  name: string
}

export type authLog = {
  email: string
  password: string
}

export type authToken = {
  token: string
}

export type addBoard = {
  id?: number
  title: string
  desc: string
}

export type Board = {
  id: number,
  title: string,
  desc: string,
  slug: string
}

export type Boards = Board[]

export type LaflirtOrder = {
  id: number,
  phone: string,
  cart: string,
  createdAt: string
  sync: boolean
  note?: string
  comment?: string
}

export type IntinShopOrder = {
  id: number,
  response: string,
  comment: string,
  createdAt: string
}

export type IntinShopOrders = IntinShopOrder[]

export type LaflirtOrders = LaflirtOrder[]


export type Rates = {
  id: number,
  value: number,
  uuid: string,
  createdAt: string | Date
}[]

export type successAddBoard = {
  boardId: number
}
