class Register {
  constructor({token}) {
    this.token = token
  }

  getToken() {
    return this.token
  }
}

export default Register
