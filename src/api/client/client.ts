import axios from 'axios'
import config from "./config";

const client = axios.create({
  ...config,
  timeout: 1000
});

export default client
