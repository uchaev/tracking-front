export const ADD_TOKEN = 'ADD_TOKEN';

export const SET_DID_TRY_AUTO_LOGIN = 'SET_DID_TRY_AUTO_LOGIN';

export const addToken = (token: string) => {
  return {type: ADD_TOKEN, token};
};

export const setDidTryAutoLogin = () => {
  return {type: SET_DID_TRY_AUTO_LOGIN};
};
