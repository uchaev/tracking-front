export type userState = {
  token: string | null
  didTryAutoLogin: boolean
}

export type AppState = {
  user: userState
}
