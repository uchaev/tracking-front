import {ADD_TOKEN, SET_DID_TRY_AUTO_LOGIN} from '../actions/user';
import {userState} from "../types/store-types";

type action = {
  type: string
  token?: string
}
const initialState: userState = {
  token: null,
  didTryAutoLogin: false,
};

const userReducer = (state = initialState, action: action) => {
  switch (action.type) {
    case ADD_TOKEN:
      return {...state, ...{token: action.token}};
    case SET_DID_TRY_AUTO_LOGIN:
      return {...state, ...{didTryAutoLogin: true}};
    default:
      return state;
  }
};

export default userReducer;
